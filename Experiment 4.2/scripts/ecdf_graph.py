#!/usr/bin/python3

import statistics
import csv
import sys
import re
import math

import numpy as np
import matplotlib.pyplot as plt

columnnames={'sha':0,
            'outcode':1,
            'runtime (s)':2,
            'memory (MB)':3,
            'instance':4,
            'decisions':5,
            'conflicts':6,
            'propagations':7,
            'LP added constraints':8,
            'LP calls':9,
            'LP pivots':10,
            'cycling count':11,
            'singular basis count':12,
            'abort count':13,
            'optimalities':14,
            'infeasibilities':15,
            'Farkas constraints':16,
            'no primal count':17,
            'no Farkas count':18,
            'basis resets':19,
            'LP iteration time (s)':20,
            'total LP overhead (s)':21,
            'LP phase differences':22,
            'LP order differences':23
            }

files=['roundingsat.csv','lp1.csv','scip.csv','sat4j.csv','sat4j_cp.csv']
x_legend=['RoundingSat','RoundingSat+SoPlex','SCIP','Sat4J','Sat4J-CP']
markers=['--','-.',':','-','|-']

families=[r"MIPLIB/.*\.0\.(d|u|s)\.opb",r"PB12/",r"PB16/",r"combinatorial/"]
series_legend=["MIPLIB","PB12","PB16", "PROOF"]
families_count=[556,122,777,1234]


def getTable(filename):
    result=[]
    with open(filename, newline='') as csvfile:
        result.extend(csv.reader(csvfile, delimiter=';', quotechar='\"'))
    return result

def isSat(row):
    return row[1]=='10'

def isUnsat(row):
    return row[1]=='20'

def hasReturned(row):
    return isSat(row) or isUnsat(row)

def isBlank(cell):
    return cell==""

def noData(row):
    return isBlank(row[columnnames['decisions']])

def containsString(row,teststring):
    for cell in row:
        if teststring in cell:
            return True
    return False

def containsRegex(row,regex):
    p = re.compile(regex)
    for cell in row:
        if p.search(cell):
            return True
    return False

def ecdf(table,cutoff,teststring,csv):
    times = []
        
    for row in table:
        if containsRegex(row,teststring):
            score=float(row[columnnames['runtime (s)']])
            outcode=int(row[columnnames['outcode']])
            if csv=='scip.csv':
                if row[5]=='[optimal':
                    outcode=10
                if row[5]=='[infeasible]':
                    outcode=20
            if (outcode==10 or outcode==20) and score<cutoff:
                times+=[score]
            else:
                times+=[cutoff*2]
    return sorted(times), range(1,len(times)+1)

###################

if len(sys.argv)!=2:
    print('Incorrect number of arguments, should be 1.')
    print('Usage: ./par2graphs.py <cutoff>')
    sys.exit(1)

cutoff=float(sys.argv[1])

#plotcols=2
#plotrows=int(math.ceil(len(families)/float(plotcols)))
#plotcounter=0

for i in range(0,len(families)):
    fam = families[i]
    #plotcounter+=1
    #ax = plt.subplot(plotrows,plotcols,plotcounter)
    fig, ax = plt.subplots()
    #fig.subplots_adjust(top=0.95)
    plt.title(series_legend[i])
    plt.xlabel('Timeout limit (s)')
    plt.ylabel('Number of solved instances')
    #ax.set_ylim([0,1])
    #plt.yticks(np.arange(0, 1.1, 0.1))
    #ax.yaxis.grid(which="major")
    ax.set_xscale('log')
    ax.set_xlim([0.01,cutoff])
    ax.set_ylim([0,families_count[i]])
    
    for j in range(0,len(files)):
        f = files[j]
        ftab = getTable(f)
        times, solveds = ecdf(ftab,cutoff,fam,f)
        ax.plot(times, solveds, markers[j])

    ax.legend(x_legend)
    fig.subplots_adjust(right=0.99)
    #fig.subplots_adjust(left=0.1)
    fig.subplots_adjust(top=0.93)
    fig.set_size_inches(5, 5)
#    fig.subplots_adjust(bottom=0.15)
    plt.savefig('/home/jodv/workspace/PB_experiments/results/summarized/abisko_other_solvers/graphs/solver_ecdf_'+series_legend[i]+'.eps')
    plt.close(fig)
