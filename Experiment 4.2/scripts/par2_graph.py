#!/usr/bin/python3

import statistics
import csv
import sys
import re

import numpy as np
import matplotlib.pyplot as plt

columnnames={'sha':0,
            'outcode':1,
            'runtime (s)':2,
            'memory (MB)':3,
            'instance':4,
            'decisions':5,
            'conflicts':6,
            'propagations':7,
            'LP added constraints':8,
            'LP calls':9,
            'LP pivots':10,
            'cycling count':11,
            'singular basis count':12,
            'abort count':13,
            'optimalities':14,
            'infeasibilities':15,
            'Farkas constraints':16,
            'no primal count':17,
            'no Farkas count':18,
            'basis resets':19,
            'LP iteration time (s)':20,
            'total LP overhead (s)':21,
            'LP phase differences':22,
            'LP order differences':23
            }

files=['roundingsat.csv','lp1.csv','scip.csv','sat4j.csv','sat4j_cp.csv']
x_legend=['\nRoundingSat','RoundingSat+SoPlex','SCIP','Sat4J','Sat4J-CP']

families=[r"MIPLIB/.*\.0\.(d|u|s)\.opb",r"PB12/",r"PB16/",r"combinatorial/"]
series_legend=["MIPLIB","PB12","PB16", "PROOF"]
markers={r"MIPLIB/.*\.0\.(d|u|s)\.opb":'o-',r"PB12/":'v-',r"PB16/":'s-',r"combinatorial/":'d-'}

def getTable(filename):
    result=[]
    with open(filename, newline='') as csvfile:
        result.extend(csv.reader(csvfile, delimiter=';', quotechar='\"'))
    return result

def isSat(row):
    return row[1]=='10'

def isUnsat(row):
    return row[1]=='20'

def hasReturned(row):
    return isSat(row) or isUnsat(row)

def isBlank(cell):
    return cell==""

def noData(row):
    return isBlank(row[columnnames['decisions']])

def containsString(row,teststring):
    for cell in row:
        if teststring in cell:
            return True
    return False

def containsRegex(row,regex):
    p = re.compile(regex)
    for cell in row:
        if p.search(cell):
            return True
    return False

def par2(table,cutoff,penalty,column,teststring,csv):
    totalScore=0.0
    worstScore=0.0
    for row in table:
        if containsRegex(row,teststring):
            worstScore+=(cutoff+penalty)
            score=float(row[columnnames[column]])
            outcode=int(row[columnnames['outcode']])
            if csv=='scip.csv':
                if row[5]=='[optimal':
                    outcode=10
                if row[5]=='[infeasible]':
                    outcode=20
            if (outcode==10 or outcode==20) and score<cutoff:
                totalScore+=score
            else:
                totalScore+=(cutoff+penalty)
    return {'total':totalScore, 'worst':worstScore}

def overhead(table,column1,column2,teststring):
    sum1=0.0
    sum2=0.00001
    for row in table:
        if containsRegex(row,teststring) and not noData(row) and not len(row)<=columnnames[column1] and not len(row)<=columnnames[column2]:
            sum1+=float(row[columnnames[column1]])
            sum2+=float(row[columnnames[column2]])
    return sum1/sum2

def count(table,teststring):
    count=0
    for row in table:
        if containsRegex(row,teststring):
            count+=1
    return count

###################

if len(sys.argv)<3 or len(sys.argv)>3:
    print('Incorrect number of arguments, should be 2.')
    print('Usage: ./par2graphs.py  <cutoff> <penalty>')
    sys.exit(1)
    
for fam in families:
    counts=[]
    for f in files:
        counts+=[count(getTable(f),fam)]
    print(fam+": ")
    print(counts)

cutoff=float(sys.argv[1])
penalty=float(sys.argv[2])

fig, ax = plt.subplots()
#plt.title('Relative PAR-2 score (lower is better)')
plt.xlabel('Solver configuration')
plt.ylabel('Relative PAR-2')
ax.set_ylim([0,1])
plt.yticks(np.arange(0, 1.1, 0.1))
ax.yaxis.grid(which="major")

for fam in families:
    values = []
    for f in files:
        ftab = getTable(f)
        scores=par2(ftab,cutoff,penalty,'runtime (s)',fam,f)
        values += [scores['total']/scores['worst']]
    print(fam+": ")
    print(values)
    ax.plot(x_legend, values, markers[fam])

ax.legend(series_legend)
fig.subplots_adjust(top=0.95)
fig.subplots_adjust(bottom=0.15)

plt.savefig('/home/jodv/workspace/PB_experiments/results/summarized/abisko_other_solvers/graphs/solver_par2.eps')
plt.close(fig)
