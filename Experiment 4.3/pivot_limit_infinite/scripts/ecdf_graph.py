#!/usr/bin/python3

import statistics
import csv
import sys
import re
import math
import inspect, os

import numpy as np
import matplotlib.pyplot as plt

columnnames={'sha':0,
            'outcode':1,
            'runtime (s)':2,
            'memory (MB)':3,
            'instance':4,
            'decisions':5,
            'conflicts':6,
            'propagations':7,
            'LP added constraints':8,
            'LP calls':9,
            'LP pivots':10,
            'cycling count':11,
            'singular basis count':12,
            'abort count':13,
            'optimalities':14,
            'infeasibilities':15,
            'Farkas constraints':16,
            'no primal count':17,
            'no Farkas count':18,
            'basis resets':19,
            'LP iteration time (s)':20,
            'total LP overhead (s)':21,
            'LP phase differences':22,
            'LP order differences':23
            }

files=['lp-1.csv','lp-1_AC.csv','lp-1_AC_tight.csv']
x_legend=['RoundingSat+SoPlex','naive','tight']
markers=['-','-.',':']

families=[r"MIPLIB/.*\.0\.(d|u|s)\.opb",r"PB12/",r"PB16/",r"combinatorial/"]
series_legend=["MIPLIB","PB12","PB16", "PROOF"]
families_count=[556,122,777,1234]
fam_markers=['o-','v-','s-','d-']


def getTable(filename):
    result=[]
    with open(filename, newline='') as csvfile:
        result.extend(csv.reader(csvfile, delimiter=';', quotechar='\"'))
    return result

def isSat(row):
    return row[1]=='10'

def isUnsat(row):
    return row[1]=='20'

def hasReturned(row):
    return isSat(row) or isUnsat(row)

def isBlank(cell):
    return cell==""

def noData(row):
    return isBlank(row[columnnames['decisions']])

def containsString(row,teststring):
    for cell in row:
        if teststring in cell:
            return True
    return False

def containsRegex(row,regex):
    p = re.compile(regex)
    for cell in row:
        if p.search(cell):
            return True
    return False

def ecdf(table,cutoff,teststring,csv):
    times = []
    for row in table:
        if containsRegex(row,teststring):
            score=float(row[columnnames['runtime (s)']])
            outcode=int(row[columnnames['outcode']])
            if csv=='scip.csv':
                if row[5]=='[optimal':
                    outcode=10
                if row[5]=='[infeasible]':
                    outcode=20
            if (outcode==10 or outcode==20) and score<cutoff:
                times+=[score]
            else:
                times+=[cutoff*2]
    return sorted(times), range(1,len(times)+1)

def ecdf_mutual(indices,table,column):
    times = []
    for i in indices:
        row = table[i]
        times+=[int(row[columnnames[column]])]
    return sorted(times), range(1,len(times)+1)

def overhead(table,column1,column2,teststring):
    sum1=0.0
    sum2=0.00001
    for row in table:
        if containsRegex(row,teststring) and not noData(row) and not len(row)<=columnnames[column1] and not len(row)<=columnnames[column2]:
            sum1+=float(row[columnnames[column1]])
            sum2+=float(row[columnnames[column2]])
    return sum1/sum2

def getMutuallySolved(indices,table):
    newindices=[]
    for i in indices:
        if hasReturned(table[i]):
            newindices+=[i]
    return newindices

###################

if len(sys.argv)!=2:
    print('Incorrect number of arguments, should be 1.')
    print('Usage: ./par2graphs.py <cutoff>')
    sys.exit(1)

cutoff=float(sys.argv[1])
current_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

#plotcols=2
#plotrows=int(math.ceil(len(families)/float(plotcols)))
#plotcounter=0

for i in range(0,len(families)):
    fam = families[i]
    #plotcounter+=1
    #ax = plt.subplot(plotrows,plotcols,plotcounter)
    fig, ax = plt.subplots()
    #fig.subplots_adjust(top=0.95)
    plt.title(series_legend[i])
    plt.xlabel('Timeout limit (s)')
    plt.ylabel('Number of solved instances')
    #ax.set_ylim([0,1])
    #plt.yticks(np.arange(0, 1.1, 0.1))
    #ax.yaxis.grid(which="major")
    ax.set_xscale('log')
    ax.set_xlim([0.01,cutoff])
    ax.set_ylim([0,families_count[i]])
    
    for j in range(0,len(files)):
        f = files[j]
        ftab = getTable(f)
        times, solveds = ecdf(ftab,cutoff,fam,f)
        ax.plot(times, solveds, markers[j])

    ax.legend(x_legend)
    fig.subplots_adjust(right=0.99)
    #fig.subplots_adjust(left=0.1)
    fig.subplots_adjust(top=0.93)
    fig.set_size_inches(5, 5)
#    fig.subplots_adjust(bottom=0.15)
    plt.savefig(current_path+'/../graphs/learnts-1_ecdf_'+series_legend[i]+'.eps')
    plt.close(fig)
    
# first, gather a list of all mutually solved instances
mutualinstances=range(0,len(getTable(files[0])))
print(str(len(mutualinstances)))
for f in files:
    mutualinstances=getMutuallySolved(mutualinstances,getTable(f))
    print(str(len(mutualinstances)))

for column in ['conflicts','LP pivots']:
    fig, ax = plt.subplots()
    #fig.subplots_adjust(top=0.95)
    plt.title('Infinite pivot ratio')
    plt.xlabel('Limit on number of '+column)
    plt.ylabel('Number of solved instances')
    #ax.set_ylim([0,1])
    #plt.yticks(np.arange(0, 1.1, 0.1))
    #ax.yaxis.grid(which="major")
    ax.set_xscale('log')
    ax.set_ylim([0,1800])

    globalmax=0
    for j in range(0,len(files)):
        f = files[j]
        ftab = getTable(f)
        times, solveds = ecdf_mutual(mutualinstances,ftab,column)
        globalmax=max(globalmax,max(times))
        ax.plot(times, solveds, markers[j])

    ax.set_xlim([1,globalmax])
    ax.legend(x_legend)
    fig.subplots_adjust(right=0.99)
    #fig.subplots_adjust(left=0.1)
    fig.subplots_adjust(top=0.93)
    #fig.set_size_inches(5, 5)
    #    fig.subplots_adjust(bottom=0.15)
    plt.savefig(current_path+'/../graphs/learnts-1_ecdf_'+column+'.eps')
    plt.close(fig)

files=['lp-1.csv','lp-1_AC_tight.csv','lp-1_AC.csv']
x_legend=['RoundingSat+SoPlex','tight','naive']
markers=['-','-.',':']

fig, ax = plt.subplots()
#plt.title('Relative LP overhead')
plt.xlabel('PB learned constraint addition technique')
plt.ylabel('LP overhead')
ax.set_ylim([0,1])
plt.yticks(np.arange(0, 1.1, 0.1))
ax.yaxis.grid(which="major")

for i in range(0,len(families)):
    fam = families[i]
    values = []
    for f in files:
        ftab = getTable(f)
        values+=[overhead(ftab,'LP iteration time (s)','runtime (s)',fam)]
    print(fam+": ")
    print(values)
    ax.plot(x_legend, values, fam_markers[i])

ax.legend(series_legend)
fig.subplots_adjust(top=0.95)
plt.savefig(current_path+'/../graphs/learnts-1_lp_overhead.eps')
plt.close(fig)
