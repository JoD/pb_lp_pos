# PB_LP_PoS

Solver binary and detailed benchmark results for the paper "Leveraging LP solving for PB solving" submitted to the Pragmatics of SAT 2019 workshop.

# How to run
`roundingsat_soplex` is a statically compiled binary that should run under any 64bit Linux distribution.  
`$ ./roundingsat_soplex` without arguments runs RoundingSat+SoPlex without SoPlex calls.  
`$ ./roundingsat_soplex --lp=k` runs RoundingSat+SoPlex with a pivot limit of `k`. If `k` equals `-1`, RoundingSat+SoPlex has an infinite pivot limit.  
`$ ./roundingsat_soplex --lp=1 --lp-addlearntsAC=1` runs RoundingSat+SoPlex with a pivot limit of `1` and with learnt clause addition to SoPlex, as described in the Pragmatics of SAT 2019 paper.  
`$ ./roundingsat_soplex --lp=1 --lp-addlearntsAC=1 --lp-usesolution=1` runs RoundingSat+SoPlex with a pivot limit of `1` and with the "tight" learnt clause addition to SoPlex, as described in the Pragmatics of SAT 2019 paper.  

# How to interpret RoundingSat+SoPlex's experimental results
All `lp_*.csv` files represent runs with RoundingSat+SoPlex, and contain the following columns:
1. A hash of the run
2. Outcode of the run (10 is SAT, 20 UNSAT)
3. Runtime in seconds
4. Memory usage in MB
5. Instance full name
6. Decisions
7. Conflicts
8. Propagations
9. Number of constraints passed to SoPlex
10. Number of SoPlex calls
11. Number of SoPlex pivots
12. Number of times SoPlex reported cycling
13. Number of times SoPlex reported a singular basis
14. Number of times SoPlex reported an unspecified abort (should not have happened)
15. Number of times SoPlex reported feasibility
16. Number of times SoPlex reported infeasibility
17. Number of valid Farkas constraints constructed
18. Number of times SoPlex returned no primal solution (should not have happened)
19. Number of times SoPlex returned no Farkas multipliers (should not have happened)
20. Number of times SoPlex's basis was re-initialized
21. SoPlex pivot overhead in seconds, as measured by SoPlex
22. Total SoPlex overhead in seconds, as measured by RoundingSat
23. Number of times RoundingSat's phase heuristic disagreed with the last rational solution (unused in the PoS paper)
24. Number of times RoundingSat's activity heuristic disagreed with the last rational solution (unused in the PoS paper)

The `summary.ods` files aggregate and visualize the results from the `*.csv` files.
